import $ from 'jquery';
import 'what-input';

// Foundation JS relies on a global variable. In ES6, all imports are hoisted
// to the top of the file so if we used `import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;
require('foundation-sites');

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();

$(window).scroll(function() {
    if ($(document).scrollTop() > 50) {
      $('#top-nav').addClass('navigation-small');
    } else {
      $('#top-nav').removeClass('navigation-small');
    }
  });

$("#scroll-down").click(function() {
  $([document.documentElement, document.body]).animate({
      scrollTop: $(".navigation").offset().top
  }, 500);
});

var n =  new Date();
var y = n.getFullYear();

document.getElementById("date").innerHTML = y;

// .validate (https://jqueryvalidation.org)
// .post (https://api.jquery.com/jQuery.post/)
// reCaptcha v3 (https://developers.google.com/recaptcha/docs/v3)
// @author Raspgot

const publicKey = "6LdO2d8ZAAAAAJ26HNvOd6kNsYF6i199T9EPuQ-T"; // GOOGLE public key


  // Website Cookie -----------------------
// ----------------------------------------
// setThisCookie()

var tnaSetThisCookie = function(name, days) {
  var d = new Date();
  d.setTime(d.getTime() + 1000 * 60 * 60 * 24 * days);
  document.cookie = name + "=true;path=/;expires=" + d.toGMTString() + ';';
};
// checkForThisCookie()
var tnaCheckForThisCookie = function(name) {
  if (document.cookie.indexOf(name) === -1) {
    return false;
  } else {
    return true;
  }
};

// Cookie notification
$(function() { // All content must be placed within this IIFE.
  $('#mega-menu-pull-down').show();
  if (!tnaCheckForThisCookie("dontShowCookieNotice")) {
    $('  <div class="privacy-notification cookieNotice"> <div class="privacy-notification-top">      <div class="privacy-notification-message">This website is using Google reCaptcha to protect against fraud and abuse.</div> <div class="privacy-notification-button"> <a   id="cookieCutter">Understood</a>  </div>    </div>    <a class="privacy-notification-link" href="privacy_policy.html">More&nbsp;details</a>  </div>').appendTo('body');

    setTimeout(function() {
      $('.cookieNotice').slideDown(1000);
    }, 1000);
  }

});

// Binding to document (event delegation)
$(document).on('click', '#cookieCutter', function(e) {
  e.preventDefault();
  tnaSetThisCookie('dontShowCookieNotice', 365);
  $('.cookieNotice').hide();
});
// ----------------------------------------